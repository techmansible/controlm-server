#!/bin/bash
EM_ACC='controlm'
# Stop the Control-M Agent
/opt/controlm/ctm_agent/ctm/scripts/shut-ag -u $EM_ACC -p ALL
# Stop the Control-M/EM Configuration Agent and all components
su - $EM_ACC -c "em ctl -mcs -C Config_Agent -all -cmd shutdown"
# Stop the Control-M/Server Configuration Agent
su - $EM_ACC -c shut_ca
# Stop the Control-M/Server
su - $EM_ACC -c shut_ctm 
# shutdown the database
su - $EM_ACC -c shutdb
