#!/bin/bash
EM_ACC='controlm'
# Start the CONTROL-M database
su - $EM_ACC -c startdb
# Start the Control-M Server
su - $EM_ACC -c start_ctm
# Start the Control-M/Server Configuration Agent
su - $EM_ACC -c start_ca
# Start the Control-M/EM Configuration Agent
su - $EM_ACC -c start_config_agent
# enable Control-M/Agent to start up as root 
/opt/controlm/ctm_agent/ctm/scripts/set_agent_mode -u controlm -o 2
/opt/controlm/ctm_agent/ctm/scripts/start-ag -u $EM_ACC -p ALL
